package gopl

// Let -
type Let struct {
	env  *Env
	lvar map[string]Form
}

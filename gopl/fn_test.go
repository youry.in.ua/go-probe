package gopl

import "testing"

func TestCSet(t *testing.T) {
	e := NewEnv()

	plist := e.PList(e.ID("cset"), e.ID("x"), e.Number(5))

	valPList := plist.Value()
	res := "5"
	if valPList.String() != res {
		t.Errorf("PList: Value '%v', must be '%v'", valPList.String(), res)
	}

	gref := e.GRef("x")
	typeGRef := gref.Type()
	if typeGRef != TGRef {
		t.Errorf("GRef: Type '%v', must be '%v'", typeGRef, TGRef)
	}

	strGRef := gref.String()
	resGRef := "GRef: x"
	if strGRef != resGRef {
		t.Errorf("GRef: String '%v', must be '%v'", strGRef, resGRef)
	}

	valGRef := gref.Value()
	if valGRef.String() != res {
		t.Errorf("GRef: Value '%v', must be '%v'", valGRef.String(), res)
	}
}

func TestSet(t *testing.T) {
	e := NewEnv().Let()

	plist := e.PList(e.ID("set"), e.ID("y"), e.Number(-5))

	valPList := plist.Value()
	res := "-5"
	if valPList.String() != res {
		t.Errorf("PList: Value '%v', must be '%v'", valPList.String(), res)
	}

	lref := e.LRef("y")
	typeLRef := lref.Type()
	if typeLRef != TLRef {
		t.Errorf("LRef: Type '%v', must be '%v'", typeLRef, TLRef)
	}

	strLRef := lref.String()
	resLRef := "LRef: y"
	if strLRef != resLRef {
		t.Errorf("LRef: String '%v', must be '%v'", strLRef, resLRef)
	}

	valLRef := lref.Value()
	if valLRef.String() != res {
		t.Errorf("LRef: Value '%v', must be '%v'", valLRef.String(), res)
	}
}

package gopl

import "testing"

func TestRun(t *testing.T) {
	e := NewEnv()
	fail := e.PList(e.ID("fail"))
	sum := e.PList(e.ID("+"), e.Number(1), e.Number(2))
	alt := e.PList(e.ID("alt"), fail, sum)
	res := Run(alt)
	t.Errorf("Run: %v (%v)", "OK", res)
}

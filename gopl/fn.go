package gopl

import "log"

func (e *Env) _alt(args ...Form) Form {
	log.Printf("ALT: args: '%v'", args)
	for _, a := range args {
		e.status = OK
		v := a.Value()
		log.Printf("E: '%v', V: '%v', S: %v", a, v, e.status)
		if e.status == OK {
			return v
		}
	}
	return e.LList()
}

func (e *Env) _fail(args ...Form) Form {
	e.status = Fail
	if len(args) > 0 {
		return args[0].Value()
	}
	return e.LList()
}

func (e *Env) _cset(args ...Form) (res Form) {
	v := args[1].Value()
	e.root.gvar[args[0].Value().String()] = v
	return v
}

func (e *Env) _set(args ...Form) (res Form) {
	v := args[1].Value()
	e.let.lvar[args[0].Value().String()] = v
	return v
}

func (e *Env) _sum(args ...Form) (res Form) {
	s := float64(0)
	for _, a := range args {
		s += a.Value().(Atom).Number().n
	}
	return e.Number(s)
}

func (e *Env) _sub(args ...Form) (res Form) {
	return e.Number(args[0].Value().(Atom).Number().n - args[1].Value().(Atom).Number().n)
}

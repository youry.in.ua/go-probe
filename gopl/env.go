package gopl

// Env -
type Env struct {
	name   string
	root   *Env
	parent *Env
	let    *Let
	status Status
	gvar   map[string]Form
	funcs  map[string]func(...Form) Form
}

// NewEnv -
func NewEnv() (e *Env) {
	e = &Env{name: "Root", parent: nil, let: nil}
	e.root = e
	e.gvar = map[string]Form{}
	e.funcs = e.initFuncs()
	return
}

// Env -
func (e *Env) Env(name string) *Env {
	return &Env{name: name, root: e.root, parent: e}
}

// Let -
func (e *Env) Let() *Env {
	let := &Let{env: e, lvar: map[string]Form{}}
	e.let = let
	return e
}

// ID -
func (e *Env) ID(text string) Form {
	return &ID{text: text}
}

// Number -
func (e *Env) Number(n float64) Form {
	return &Number{n: n}
}

// LList -
func (e *Env) LList(list ...Form) Form {
	return &LList{env: e, list: list}
}

// PList -
func (e *Env) PList(fn Form, args ...Form) Form {
	return &PList{env: e, fn: fn, args: args}
}

// GRef -
func (e *Env) GRef(text string) Form {
	return &GRef{env: e, text: text}
}

// LRef -
func (e *Env) LRef(text string) Form {
	return &LRef{env: e, text: text}
}

func (e *Env) initFuncs() map[string]func(...Form) Form {
	return map[string]func(...Form) Form{
		"alt":  e._alt,
		"fail": e._fail,
		"cset": e._cset,
		"set":  e._set,
		"+":    e._sum,
		"-":    e._sub,
	}
}

package gopl

import "testing"

func TestLList(t *testing.T) {
	e := NewEnv()

	llist := e.LList(e.ID("a"), e.ID("b"), e.ID("c"))
	typeLList := llist.Type()
	if typeLList != TLList {
		t.Errorf("LList: Type '%v', must be '%v'", typeLList, TLList)
	}

	strLList := llist.String()
	res := "LList: [a b c]"
	if strLList != res {
		t.Errorf("LList: String '%v', must be '%v'", strLList, res)
	}

}

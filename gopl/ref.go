package gopl

import "fmt"

// GRef -
type GRef struct {
	Form
	env  *Env
	text string
}

// Type -
func (gr *GRef) Type() FormType {
	return TGRef
}

// Value -
func (gr *GRef) Value() Form {
	if val, ok := gr.env.root.gvar[gr.text]; ok {
		return val
	}
	panic(fmt.Sprintf("Global reference '%v' not defined", gr.text))
}

// String -
func (gr *GRef) String() string {
	return fmt.Sprintf("GRef: %v", gr.text)
}

// LRef -
type LRef struct {
	Form
	env  *Env
	text string
}

// Type -
func (lr *LRef) Type() FormType {
	return TLRef
}

// Value -
func (lr *LRef) Value() Form {
	e := lr.env
	for e != nil {
		if val, ok := e.let.lvar[lr.text]; ok {
			return val
		}
		e = e.parent
	}
	panic(fmt.Sprintf("Local reference '%v' not defined", lr.text))
}

// String -
func (lr *LRef) String() string {
	return fmt.Sprintf("LRef: %v", lr.text)
}

package gopl

import "testing"

func TestAtom(t *testing.T) {
	e := NewEnv()

	id := e.ID("abc")
	typeID := id.Type()
	if typeID != TAtom {
		t.Errorf("ID: Type '%v', must be '%v'", typeID, TAtom)
	}

	atypeID := id.(Atom).AtomType()
	if atypeID != ATID {
		t.Errorf("ID: AtomType '%v', must be '%v'", atypeID, ATID)
	}

	strID := id.String()
	resID := "abc"
	if strID != resID {
		t.Errorf("ID: String '%v', must be '%v'", strID, resID)
	}

	num := e.Number(1)
	typeNum := num.Type()
	if typeNum != TAtom {
		t.Errorf("Number: Type '%v', must be '%v'", typeNum, TAtom)
	}

	atypeNum := num.(Atom).AtomType()
	if atypeNum != ATNumber {
		t.Errorf("Number: AtomType '%v', must be '%v'", atypeNum, ATNumber)
	}

	strNum := num.String()
	resNum := "1"
	if strNum != resNum {
		t.Errorf("Num: String '%v', must be '%v'", strNum, resNum)
	}

}

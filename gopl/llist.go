package gopl

import "fmt"

// LList -
type LList struct {
	Form
	env  *Env
	list []Form
}

// Type -
func (ll *LList) Type() FormType {
	return TLList
}

// Value -
func (ll *LList) Value() Form {

	list := []Form{}

	for _, e := range ll.list {
		list = append(list, e.Value())
	}

	return ll.env.LList(list...)
}

// String -
func (ll *LList) String() string {
	return fmt.Sprintf("LList: %v", ll.list)
}

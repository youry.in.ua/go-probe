package gopl

import "testing"

func TestPList(t *testing.T) {
	e := NewEnv()

	plist := e.PList(e.ID("+"), e.Number(1), e.Number(2))
	typePList := plist.Type()
	if typePList != TPList {
		t.Errorf("PList: Type '%v', must be '%v'", typePList, TPList)
	}

	strPList := plist.String()
	res := "PList: + [1 2]"
	if strPList != res {
		t.Errorf("PList: String '%v', must be '%v'", strPList, res)
	}

	valPList := plist.Value()
	res = "3"
	if valPList.String() != res {
		t.Errorf("PList: Value '%v', must be '%v'", valPList.String(), res)
	}

	plist = e.PList(e.ID("-"), valPList, e.Number(3))
	valPList = plist.Value()
	res = "0"
	if valPList.String() != res {
		t.Errorf("PList: Value '%v', must be '%v'", valPList.String(), res)
	}
}

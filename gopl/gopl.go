package gopl

// Status -
type Status int

// Status enum
const (
	OK Status = iota
	Fail
)

func (s Status) String() (res string) {
	switch s {
	case OK:
		res = "OK"
	case Fail:
		res = "FAIL"
	default:
		res = "Status Undefined"
	}
	return
}

// FormType -
type FormType int

// FormType enum
const (
	TAtom  FormType = iota // Atom - id, number, bit-string
	TLList                 // List
	TPList                 // Function call
	TLRef                  // Local reference
	TGRef                  // Global reference
	TSLRef                 // Segmental local reference
	TSGRef                 // Segmental global reference
	TSList                 // Segmental function call
)

func (ft FormType) String() (res string) {
	switch ft {
	case TAtom:
		res = "TAtom"
	case TLList:
		res = "TLList"
	case TPList:
		res = "TPList"
	case TLRef:
		res = "TLRef"
	case TGRef:
		res = "TGRef"
	case TSLRef:
		res = "TSLRef"
	case TSGRef:
		res = "TSGRef"
	case TSList:
		res = "TSList"
	default:
		res = "TUndefined"
	}
	return
}

// Form -
type Form interface {
	Type() FormType
	Value() Form
	String() string
}

// Run -
func Run(form Form) Form {
	return form.Value()
}

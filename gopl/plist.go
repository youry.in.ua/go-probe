package gopl

import (
	"fmt"
)

// PList -
type PList struct {
	Form
	env  *Env
	fn   Form
	args []Form
}

// Type -
func (pl *PList) Type() FormType {
	return TPList
}

// Value -
func (pl *PList) Value() Form {
	id := pl.fn.Value().String()
	//log.Println(id, pl.args)
	if fn, ok := pl.env.funcs[id]; ok {
		return fn(pl.args...)
	}
	panic(fmt.Sprintf("Function '%v' not defined", id))
}

// String -
func (pl *PList) String() string {
	return fmt.Sprintf("PList: %v %v", pl.fn, pl.args)
}

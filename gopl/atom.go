package gopl

import "fmt"

// AtomType -
type AtomType int

// AtomType enum
const (
	ATID     AtomType = iota // id
	ATNumber                 // Number
	ATBits                   // Bit string
)

func (at AtomType) String() (res string) {
	switch at {
	case ATID:
		res = "ATID"
	case ATNumber:
		res = "ATNumber"
	case ATBits:
		res = "ATBits"
	default:
		res = "ATUndefined"
	}
	return
}

// Atom -
type Atom interface {
	Form
	AtomType() AtomType
	Number() *Number
	ID() *ID
}

// ID -
type ID struct {
	Atom
	env  *Env
	text string
}

// AtomType -
func (id *ID) AtomType() AtomType {
	return ATID
}

// ID -
func (id *ID) ID() *ID {
	return id
}

// Number -
func (id *ID) Number() *Number {
	panic("Call method Number to ID struct")
}

// Type -
func (id *ID) Type() FormType {
	return TAtom
}

// Value -
func (id *ID) Value() Form {
	return id
}

// String -
func (id *ID) String() string {
	return id.text
}

// Number -
type Number struct {
	Atom
	env *Env
	n   float64
}

// AtomType -
func (n *Number) AtomType() AtomType {
	return ATNumber
}

// ID -
func (n *Number) ID() *ID {
	panic("Call method ID to Number struct")
}

// Number -
func (n *Number) Number() *Number {
	return n
}

// Type -
func (n *Number) Type() FormType {
	return TAtom
}

// Value -
func (n *Number) Value() Form {
	return n
}

// String -
func (n *Number) String() string {
	return fmt.Sprintf("%v", n.n)
}

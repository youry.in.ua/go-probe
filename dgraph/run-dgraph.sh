docker rmi $(docker images -f "dangling=true" -q)
docker rm -f diggy
docker run -it -p 5080:5080 -p 6080:6080 -p 8080:8080 -p 9080:9080 -p 8000:8000 -v /opt/dgraph:/dgraph --name diggy dgraph/dgraph dgraph zero &
docker exec -it diggy dgraph server --memory_mb 2048 --zero localhost:5080 &
docker exec -it diggy dgraph-ratel &
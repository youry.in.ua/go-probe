package eventsrc

import (
	"fmt"
)

////////////
// Entity
////////////

// Entity -
type Entity interface {
	Ident() Ident
	String() string
}

////////////
// NameEntity -
////////////

// NameEntity -
type NameEntity struct {
	Entity
	ID   Ident
	Name string
}

// NewNameEntity -
func NewNameEntity(name string) Entity {
	return &NameEntity{
		ID:   *NewIdent(),
		Name: name,
	}
}

// Ident -
func (ne NameEntity) Ident() Ident {
	return ne.ID
}

// String -
func (ne NameEntity) String() string {
	return fmt.Sprintf("{Named Entity: %s, %s}", ne.Ident().String(), ne.Name)
}

////////////
// ObjectEntity -
////////////

// ObjectEntity -
type ObjectEntity struct {
	Entity
	ID Ident
}

// NewObjectEntity -
func NewObjectEntity() Entity {
	return &ObjectEntity{
		ID: *NewIdent(),
	}
}

// Ident -
func (oe ObjectEntity) Ident() Ident {
	return oe.ID
}

// String -
func (oe ObjectEntity) String() string {
	return fmt.Sprintf("{Object Entity: %s}", oe.Ident().String())
}

////////////
// PropEntity -
////////////

// PropEntity -
type PropEntity struct {
	Entity
	ID    Ident
	Name  *NameEntity
	Value Entity
}

// NewPropEntity -
func NewPropEntity(name NameEntity, value Entity) Entity {
	return &PropEntity{
		ID:    *NewIdent(),
		Name:  &name,
		Value: value,
	}
}

// Ident -
func (pe PropEntity) Ident() Ident {
	return pe.ID
}

// String -
func (pe PropEntity) String() string {
	return fmt.Sprintf("{Prop Entity: %s, %s = %s}", pe.Ident().String(), pe.Name.String(), pe.Value.String())
}

////////////
// LinkEntity -
////////////

// LinkEntity -
type LinkEntity struct {
	Entity
	ID   Ident
	From Entity
	To   Entity
}

// NewLinkEntity -
func NewLinkEntity(from Entity, to Entity) Entity {
	return &LinkEntity{
		ID:   *NewIdent(),
		From: from,
		To:   to,
	}
}

// Ident -
func (le LinkEntity) Ident() Ident {
	return le.ID
}

// String -
func (le LinkEntity) String() string {
	return fmt.Sprintf("{Link Entity: %s, %s => %s}", le.Ident().String(), le.From.String(), le.To.String())
}

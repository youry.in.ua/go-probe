package eventsrc

import (
	"log"
	"os"
	"testing"
	"time"
)

func TestEventSource(t *testing.T) {

	file := "test.bolt"
	os.Remove(file)

	es := NewEventSource(file, 0755)
	//log.Println(es)
	i1 := NewIdent()
	o1 := NewObjectEntity()
	e1 := CreateEvent(*i1, time.Now().Unix(), CreateEntity, o1, nil)

	log.Println("e1:", e1.Ident())

	if err := es.Add(*e1).Error(); err != nil {
		t.Errorf("Add: %v", err)
	}

	e2 := NewEvent()
	log.Println("e2:", e2.Ident())

	if err := es.Get(e2.Ident().Bytes(), e2).Error(); err == nil {
		t.Errorf("Get: must be key not found")
	}

	if err := es.CleanError().Get(e1.Ident().Bytes(), e2).Error(); err != nil {
		t.Errorf("Get: %v", err)
	}

	log.Println("e2 after Get e1", e2.Ident())

	if !e1.Ident().IsEqual(e2.Ident()) {
		t.Errorf("Idents must be equal")
	}

	var ae = []Event{}
	if err := es.CleanError().GetForBase(e1.GetBase(), &ae).Error(); err != nil {
		t.Errorf("GetForBase: %v", err)
	}

	log.Println("ae:", ae)

	if len(ae) != 1 {
		t.Errorf("Array must have one item")
	}

	aevs := []Event{
		*NewEvent().SetType(CreateEntity).SetBase(NewNameEntity("entity1")),
		*NewEvent().SetType(CreateEntity).SetBase(NewNameEntity("entity2")),
		*NewEvent().SetType(CreateEntity).SetBase(NewNameEntity("entity3")),
		*NewEvent().SetType(CreateEntity).SetBase(NewNameEntity("entity4")),
	}

	for _, e := range aevs {
		if err := es.Add(e).Error(); err != nil {
			t.Errorf("Add: %v", err)
		}
	}

	ae = []Event{}
	if err := es.CleanError().GetForBase(e1.GetBase(), &ae).Error(); err != nil {
		t.Errorf("GetForBase: %v", err)
	}

	log.Println("ae:", ae)
	if len(ae) != 1 {
		t.Errorf("Array must have one item")
	}

	ne5 := NewNameEntity("entity5")
	aevs = []Event{
		*NewEvent().SetType(CreateEntity).SetBase(ne5),
		*NewEvent().SetType(SetProperty).SetBase(ne5),
		*NewEvent().SetType(LinkEntities).SetBase(ne5),
		*NewEvent().SetType(ChangeState).SetBase(ne5),
	}

	for _, e := range aevs {
		if err := es.Add(e).Error(); err != nil {
			t.Errorf("Add: %v", err)
		}
	}

	ae = []Event{}
	if err := es.CleanError().GetForBase(ne5, &ae).Error(); err != nil {
		t.Errorf("GetForBase: %v", err)
	}

	log.Println("ae:", ae)
	if len(ae) != 4 {
		t.Errorf("Array must have four items")
	}

	//t.Errorf("*")
}

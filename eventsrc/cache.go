package eventsrc

////////////
// ObjectState
////////////

// ObjectState -
type ObjectState int8

//  ObjectState constants
const (
	NullState ObjectState = iota
	Created
	Activated
	Deactivated
	Droped
)

// ObjectStateString -
var ObjectStateString = map[ObjectState]string{
	NullState:   "NullState",
	Created:     "Created",
	Activated:   "Activated",
	Deactivated: "Deactivated",
	Droped:      "Droped",
}

func (ost ObjectState) String() string {

	if s, ok := ObjectStateString[ost]; ok {
		return s
	}

	return "Undefined Object State"
}

// Object -
type Object struct {
	Base    ObjectEntity
	State   ObjectState
	Props   map[NameEntity]PropEntity
	RelTo   map[NameEntity]LinkEntity
	RelFrom map[NameEntity]LinkEntity
}

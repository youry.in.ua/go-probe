package eventsrc

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"log"
	"os"

	"github.com/boltdb/bolt"
)

// EventSource -
type EventSource struct {
	db *bolt.DB

	src []byte
	bi  []byte
	err error
}

// NewEventSource -
func NewEventSource(path string, mode os.FileMode) *EventSource {

	gob.Register(NameEntity{})
	gob.Register(ObjectEntity{})
	gob.Register(PropEntity{})
	gob.Register(LinkEntity{})

	root := "/"
	bi := "/bi"
	db, err := bolt.Open(path, mode, nil)
	if err == nil {
		db.Update(func(tx *bolt.Tx) error {
			_, err = tx.CreateBucketIfNotExists([]byte(root))
			if err != nil {
				return fmt.Errorf("create bucket %s: %s", root, err)
			}
			_, err = tx.CreateBucketIfNotExists([]byte(bi))
			if err != nil {
				return fmt.Errorf("create bucket %s: %s", bi, err)
			}
			return nil
		})
	}

	return &EventSource{
		db:  db,
		src: []byte(root),
		bi:  []byte(bi),
		err: err,
	}
}

// Add -
func (es *EventSource) Add(ev Event) *EventSource {
	var buf bytes.Buffer

	if es.err != nil {
		return es
	}

	enc := gob.NewEncoder(&buf)
	if es.err = enc.Encode(ev); es.err != nil {
		return es
	}

	es.err = es.db.Update(func(tx *bolt.Tx) error {

		b := tx.Bucket(es.src)
		bi := tx.Bucket(es.bi)

		//log.Println(ev.Ident().Bytes())

		err := b.Put(ev.Ident().Bytes(), buf.Bytes())

		err = es.updateBaseIndex(bi, ev)
		//log.Println(err)
		return err
	})

	return es
}

// Get -
func (es *EventSource) Get(key []byte, ev *Event) *EventSource {
	var buf bytes.Buffer

	if es.err != nil {
		return es
	}

	dec := gob.NewDecoder(&buf)

	es.err = es.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(es.src)
		//log.Println(ev.Ident().Bytes())
		bs := b.Get(key)
		if bs == nil {
			return fmt.Errorf("Evenr Source - Get: key %v not found", key)
		}
		_, err := buf.Write(bs)
		return err
	})

	if es.err == nil {
		es.err = dec.Decode(ev)
	}

	return es
}

// GetForBase -
func (es *EventSource) GetForBase(base Entity, evs *[]Event) *EventSource {

	var ev Event
	//ae := []Event{}

	if es.err != nil {
		return es
	}

	es.err = es.db.View(func(tx *bolt.Tx) error {
		var buf bytes.Buffer

		b := tx.Bucket(es.src)
		//log.Println(ev.Ident().Bytes())
		c := b.Cursor()

		if c == nil {
			return fmt.Errorf("Evenr Source - GetForBase: cursor not open")
		}

		dec := gob.NewDecoder(&buf)

		for k, v := c.First(); k != nil; k, v = c.Next() {
			//fmt.Printf("key=%s, value=%s\n", k, v)

			if _, err := buf.Write(v); err != nil {
				log.Println(err)
				return err
			}
			ev = Event{}
			if err := dec.Decode(&ev); err != nil {
				log.Println(err)
				continue
			}
			//log.Println(ev)

			i := ev.GetBase().Ident()
			//log.Println(i, base.Ident())

			if i.IsEqual(base.Ident()) {

				//log.Println(ev.Ident())

				*evs = append(*evs, ev)

			}

			//log.Println(*evs)

			dec = gob.NewDecoder(&buf)
		}

		return nil
	})

	//evs = &ae
	//log.Println(*evs)
	return es
}

// updateBaseIndex -
func (es *EventSource) updateBaseIndex(bi *bolt.Bucket, ev Event) error {

	//log.Println("upadteBaseIndex ev.GetBase():", ev.GetBase())

	key := ev.GetBase().Ident().Bytes()

	v := bi.Get(key)
	if v == nil {
		v = make([]byte, 0)
	}

	l := len(v)

	//log.Println("upadteBaseIndex v:", v)

	nv := make([]byte, l+16)
	copy(nv, v)
	copy(nv[l:], ev.Ident().Bytes())

	//log.Println("upadteBaseIndex nv:", nv)

	es.err = bi.Put(key, nv)

	return es.err
}

// CleanError -
func (es *EventSource) CleanError() *EventSource {
	es.err = nil
	return es
}

// Error -
func (es *EventSource) Error() error {
	return es.err
}

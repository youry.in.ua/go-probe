package eventsrc

import (
	"fmt"
	"time"

	"github.com/youryharchenko/guid"
)

////////////
// Ident
////////////

// Ident -
type Ident struct {
	GUID guid.Guid
	err  error
}

// NewIdent -
func NewIdent() *Ident {
	return &Ident{
		GUID: *guid.New(),
	}
}

// CreateIdent -
func CreateIdent(bs []byte) *Ident {
	if len(bs) != 16 {
		return nil
	}

	var b16 [16]byte
	copy(b16[:], bs)

	return &Ident{
		GUID: guid.Guid(b16),
	}
}

// Clone -
func (i Ident) Clone() *Ident {
	c := &Ident{}
	return c.Parse(i.String())
}

// String -
func (i Ident) String() string {
	return i.GUID.String()
}

// Bytes -
func (i Ident) Bytes() []byte {
	bs := [16]byte(i.GUID)
	return bs[:]
}

// Parse -
func (i *Ident) Parse(s string) *Ident {
	var g *guid.Guid

	g, i.err = guid.ParseString(s)
	if i.err == nil {
		i.GUID = *g
		return i
	}

	return i
}

// Error -
func (i Ident) Error() error {
	return i.err
}

// IsEqual -
func (i Ident) IsEqual(j Ident) bool {
	bi := [16]byte(i.GUID)
	bj := [16]byte(j.GUID)

	for n, b := range bi {
		if bj[n] != b {
			return false
		}
	}

	return true
}

////////////
// Event
////////////

// Event -
type Event struct {
	ID    Ident
	Ts    int64
	T     EventType
	Base  Entity
	Attrs []Entity
	err   error
}

// NewEvent -
func NewEvent() *Event {
	return &Event{
		ID: *NewIdent(),
		Ts: time.Now().UTC().Unix(),
	}
}

// CreateEvent -
func CreateEvent(i Ident, ts int64, t EventType, b Entity, ps []Entity) *Event {
	return &Event{
		ID:    i,
		Ts:    ts,
		T:     t,
		Base:  b,
		Attrs: ps,
	}
}

// SetType -
func (ev *Event) SetType(t EventType) *Event {
	ev.T = t
	return ev
}

// GetType -
func (ev Event) GetType() EventType {
	return ev.T
}

// SetBase -
func (ev *Event) SetBase(b Entity) *Event {
	ev.Base = b
	return ev
}

// GetBase -
func (ev Event) GetBase() Entity {
	return ev.Base
}

// SetProperties -
func (ev *Event) SetProperties(as []Entity) *Event {
	ev.Attrs = as
	return ev
}

// GetAttrs -
func (ev Event) GetAttrs() []Entity {
	return ev.Attrs
}

// Clone -
func (ev Event) Clone() *Event {
	return CreateEvent(*ev.Ident().Clone(), ev.Ts, ev.GetType(), ev.GetBase(), ev.GetAttrs())
}

// Ident -
func (ev Event) Ident() Ident {
	return ev.ID
}

// Timestamp -
func (ev Event) Timestamp() time.Time {
	return time.Unix(ev.Ts, 0).UTC()
}

// String -
func (ev Event) String() string {
	return fmt.Sprintf("{%v: %s %s %s}", ev.T, ev.Ident().String(), ev.Timestamp().String(), ev.GetBase().String())
}

func (ev Event) Error() error {
	return ev.err
}

////////////
// EventType
////////////

// EventType -
type EventType int8

// EventType constants
const (
	NullEvent EventType = iota
	CreateEntity
	DropEntity
	ChangeState
	SetProperty
	CleanProperty
	LinkEntities
	UnlinkEntities
)

// EventTypeString -
var EventTypeString = map[EventType]string{
	NullEvent:      "NullEvent",
	CreateEntity:   "CreateEntity",
	DropEntity:     "DropEntity",
	ChangeState:    "ChangeState",
	SetProperty:    "SetProperty",
	CleanProperty:  "CleanProperty",
	LinkEntities:   "LinkEntities",
	UnlinkEntities: "UnlinkEntities",
}

func (et EventType) String() string {

	if s, ok := EventTypeString[et]; ok {
		return s
	}

	return "Undefined Event Type"
}

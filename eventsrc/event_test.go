package eventsrc

import (
	"testing"
	"time"
)

func TestEvent(t *testing.T) {
	e := NewEvent()
	c := e.Clone()

	/*
			s := e.String()
			if len(NewEvent().String()) != 66 {
				t.Errorf("NewEvent: String: %s, Length must be %d, but it is %d", s, 66, len(s))
			}

			if len(c.String()) != 66 {
				t.Errorf("Clone: String: %s, Length must be %d, but it is %d", s, 66, len(s))
			}


		if c.Ident() == e.Ident() {
			t.Errorf("== Ident: Source and Clone can not be ==")
		}

	*/

	if !c.Ident().IsEqual(e.Ident()) {
		t.Errorf("IsEqual Ident: Source (%s) and Clone (%s) must be equal", e.Ident().String(), c.Ident().String())
	}

	if NewEvent().Ident().IsEqual(NewEvent().Ident()) {
		t.Errorf("IsEqual Ident: Two NewEvent events must be not equal")
	}

	i := NewIdent()
	err := i.Parse("aaa").Error()
	if err == nil {
		t.Errorf("Parse Ident: Error must be not nil, %s", err)
	}

	if e.GetType() != NullEvent {
		t.Errorf("GetType: EventType must be %s, but it is %s", NullEvent.String(), e.GetType().String())
	}

	if e.SetType(CreateEntity).GetType() != CreateEntity {
		t.Errorf("SetType: EventType must be %s, but it is %s", CreateEntity.String(), e.GetType().String())
	}

	o1 := NewObjectEntity()
	e1 := NewEvent().SetType(CreateEntity).SetBase(o1)
	if e1.GetBase().String() != o1.String() {
		t.Errorf("Event: %s", e1.String())
		t.Errorf("SetBase: Base must be %s, but it is %s", o1.String(), e1.GetBase().String())
	}

	i2 := NewIdent()
	o2 := NewObjectEntity()
	e2 := CreateEvent(*i2, time.Now().Unix(), CreateEntity, o2, nil)
	if !e2.GetBase().Ident().IsEqual(o2.Ident()) {
		t.Errorf("Event: %s", e2.String())
		t.Errorf("GetBase: Base must be %s, but it is %s", o2.Ident().String(), e2.GetBase().Ident().String())
	}

}

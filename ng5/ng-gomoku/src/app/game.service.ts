import { Injectable } from '@angular/core';
import * as ex from 'excalibur';

import { Game } from './game'

const 
  range = max => Array.from(new Array(max), (_, i) => i),
  d = 31,
  n = 15,
  p = 8,
  //py = 27,
  //ur = 12,
  //l = d * n,
  width = d * n + d + (p + 1) * 2,
  height = d * n + d + (p * 2 + 1 + 35) + (p * 2 + 1 + 18),
  cx = width/2,
  cy = height/2,
  top = cy - 7 * d,
  bot = cy + 7 * d,
  left = cx - 7 * d,
  right = cx + 7 * d,
  a = range(15),
  ax = a.map(x => left + x * d),
  ay = a.map(x => top + x * d);


@Injectable()
export class GameService {

  constructor() { 
    
  }

  engine: ex.Engine;
  //game: Game;

  start(): void {
    this.engine = new ex.Engine({
      canvasElementId: "game",
      width: width,
      height: height, 
      backgroundColor: ex.Color.LightGray
    });

    var game = new Game();
    var engine = this.engine;
    var scene = new ex.Scene();
    
    var getCoord = function(x: number, y: number): any {
      var dd = d/2;

      for (var i of a) {
        if (x > ax[i] - dd && x < ax[i] + dd ) {
          for (var j of a) {
            if (y > ay[j] - dd && y < ay[j] + dd ) {
              return {x: i, y: j};        
            }
          }
        }
      }

      return {x: -1, y: -1};
    }

    var stepDraw = function(ctx, d) {
      console.log("draw step");
     
      ctx.fillStyle = this.color.toString();
      
      ctx.beginPath();
      ctx.arc(this.pos.x, this.pos.y, d/2, 0, Math.PI * 2);
      ctx.closePath();
      ctx.fill;     
      
    }

    var grid = new ex.Actor(0, 0, this.engine.canvasWidth, this.engine.canvasHeight, ex.Color.Black);
    grid.draw = function(ctx, d) {
      //console.log("draw grid");
     
      ctx.strokeStyle = this.color.toString();
      ctx.strokeRect(0, 0, width, height);
     
      ctx.beginPath();
      for (var x of ax) {
        ctx.moveTo(x, top);
        ctx.lineTo(x, bot);
      }
      for (var y of ay) {
        ctx.moveTo(left, y);
        ctx.lineTo(right, y);
      }
     
      ctx.closePath();
      ctx.stroke();
    }

    //var bSteps = new ex.Actor(0, 0, this.engine.canvasWidth, this.engine.canvasHeight, ex.Color.Black);
    
    grid.on("pointerup", function(evnt) {
      var co = getCoord(evnt.x, evnt.y);
      if (evnt.button == 0) {
        //console.log("pointerup: "+ evnt.button + " (" + co.x + "," + co.y + ")");
        addStep(co.x, co.y);
      }

    });

    var addStep = function(x: number, y: number) {
      var step: ex.Actor;
      if (x > -1 && y > -1) {

        if (game.nstep % 2 == 0) {
          step = new ex.Actor(0, 0, ax[x], ay[y], ex.Color.Black);
        } else {
          step  = new ex.Actor(0, 0, ax[x], ay[y], ex.Color.White);
        }
        step.draw = stepDraw;

        grid.add(step);
        
        game.addStep(x, y);
      }
      
    }

    scene.add(grid);

    engine.add("root", scene);
    engine.goToScene("root");

    engine.start();
  }

}

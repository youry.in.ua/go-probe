import { Component, OnInit } from '@angular/core';


import { GameService } from '../game.service';

@Component({
  selector: 'app-desk',
  templateUrl: './desk.component.html',
  styleUrls: ['./desk.component.css']
})
export class DeskComponent implements OnInit {

  constructor(private gameService: GameService) { }

  gameStart(): void {
    this.gameService.start();
  }

  ngOnInit() {
    this.gameStart();
  }

}

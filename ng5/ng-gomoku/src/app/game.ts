export class Game {
    nstep: number = 0;
    steps: any[] = [];

    addStep(x: number, y: number) {
        console.log("add step: " + this.nstep % 2 + " (" + x + "," + y + "), n: " + this.nstep );
        this.nstep = this.steps.push({x: x, y: y, n: this.nstep});
       
    } 
    
    getSteps(): any[] {
        return this.steps;
    }
}
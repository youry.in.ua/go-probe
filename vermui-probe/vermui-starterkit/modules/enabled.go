package modules

import (
	"gitlab.com/youry.in.ua/go-probe/tview"

	"github.com/hofstadter-io/connector-go"

	"gitlab.com/youry.in.ua/go-probe/vermui-probe/vermui-starterkit/modules/demos"
	"gitlab.com/youry.in.ua/go-probe/vermui-probe/vermui-starterkit/modules/help"
	"gitlab.com/youry.in.ua/go-probe/vermui-probe/vermui-starterkit/modules/home"
	"gitlab.com/youry.in.ua/go-probe/vermui-probe/vermui-starterkit/modules/root"
)

var (
	Module   connector.Connector
	rootView tview.Primitive
)

func Init() {
	rootView = root.New()

	items := []interface{}{
		// primary layout components
		rootView,

		// routable pages
		home.New(),
		help.New(),
		demos.New(),
	}

	conn := connector.New("root")
	conn.Add(items)
	Module = conn

	Module.Connect(Module)
}

func RootView() tview.Primitive {
	return rootView
}

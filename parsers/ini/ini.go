package main

import (
	"strings"

	"github.com/alecthomas/participle"
	"github.com/alecthomas/participle/lexer"
	"github.com/alecthomas/repr"
)

// A custom lexer for INI files. This illustrates a relatively complex Regexp lexer, as well
// as use of the Unquote filter, which unquotes string tokens.
var iniLexer = lexer.Must(lexer.Regexp(
	`(?m)` +
		`(\s+)` +
		`|(^[#;].*$)` +
		`|(?P<Ident>[a-zA-Z][a-zA-Z_\d]*)` +
		`|(?P<String>"(?:\\.|[^"])*")` +
		`|(?P<Float>\d+(?:\.\d+)?)` +
		`|(?P<Punct>[][=])`,
))

var example = `
a = "a"
b = 123

# A comment
[numbers]
a = 10.3
b = 20

; Another comment
[strings]
a = "\"quoted\""
b = "b"
`

type INI struct {
	Pos        lexer.Position
	Properties []*Property `{ @@ }`
	Sections   []*Section  `{ @@ }`
}

type Section struct {
	Pos        lexer.Position
	Identifier string      `"[" @Ident "]"`
	Properties []*Property `{ @@ }`
}

type Property struct {
	Pos   lexer.Position
	Key   string `@Ident "="`
	Value *Value `@@`
}

type Value struct {
	Pos    lexer.Position
	String *string  `  @String`
	Number *float64 `| @Float`
}

func main() {
	parser, err := participle.Build(&INI{}, participle.Lexer(iniLexer), participle.Unquote(iniLexer, "String"))
	if err != nil {
		panic(err)
	}
	ini := &INI{}
	//err = parser.Parse(os.Stdin, ini)
	err = parser.Parse(strings.NewReader(example), ini)
	if err != nil {
		panic(err)
	}
	repr.Println(ini, repr.Indent("  "), repr.OmitEmpty(true))
}

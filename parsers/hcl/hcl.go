// Package main implements a parser for HashiCorp's HCL configuration syntax.
package main

import (
	"fmt"
	"strings"

	"gopkg.in/alecthomas/kingpin.v2"

	"github.com/alecthomas/participle"
	"github.com/alecthomas/repr"
)

var example = `
region = "us-west-2"
access_key = "something"
secret_key = "something_else"
bucket = "backups"

directory config {
    source_dir = "/etc/eventstore"
    dest_prefix = "escluster/config"
    exclude = ["*.hcl"]
    pre_backup_script = "before_backup.sh"
    post_backup_script = "after_backup.sh"
    pre_restore_script = "before_restore.sh"
    post_restore_script = "after_restore.sh"
    chmod = 0755
}

directory data {
    source_dir = "/var/lib/eventstore"
    dest_prefix = "escluster/a/data"
    exclude = [
        "*.merging"
    ]
    pre_restore_script = "before_restore.sh"
    post_restore_script = "after_restore.sh"
}
`

type Bool bool

func (b *Bool) Capture(v []string) error { *b = v[0] == "true"; return nil }

type Value struct {
	Boolean    *Bool    `  @("true"|"false")`
	Identifier *string  `| @Ident { @"." @Ident }`
	String     *string  `| @(String|Char|RawString)`
	Number     *float64 `| @(Float|Int)`
	Array      []*Value `| "[" { @@ [ "," ] } "]"`
}

func (l *Value) GoString() string {
	switch {
	case l.Boolean != nil:
		return fmt.Sprintf("%v", *l.Boolean)
	case l.Identifier != nil:
		return fmt.Sprintf("`%s`", *l.Identifier)
	case l.String != nil:
		return fmt.Sprintf("%q", *l.String)
	case l.Number != nil:
		return fmt.Sprintf("%v", *l.Number)
	case l.Array != nil:
		out := []string{}
		for _, v := range l.Array {
			out = append(out, v.GoString())
		}
		return fmt.Sprintf("[]*Value{ %s }", strings.Join(out, ", "))
	}
	panic("??")
}

type Entry struct {
	Key   string `@Ident`
	Value *Value `( "=" @@`
	Block *Block `| @@ )`
}

type Block struct {
	Parameters []*Value `{ @@ }`
	Entries    []*Entry `"{" { @@ } "}"`
}

type Config struct {
	Entries []*Entry `{ @@ }`
}

func main() {
	kingpin.Parse()

	parser, err := participle.Build(&Config{})
	kingpin.FatalIfError(err, "")

	expr := &Config{}
	err = parser.Parse(strings.NewReader(example), expr)
	kingpin.FatalIfError(err, "")

	repr.Println(expr)
}

package datum

import (
	"log"
	"testing"
	"time"

	"github.com/youryharchenko/guid"

	_ "github.com/lib/pq"
	//"fmt"
)

var createEntities = `CREATE TABLE d_entities
(
  uuid text not null,
  created_at timestamp with time zone,
  updated_at timestamp with time zone,
  deleted_at timestamp with time zone,
  name text not null,
  CONSTRAINT d_entities_pkey PRIMARY KEY (uuid)
)`

var dropEntities = `DROP TABLE d_entities`

var insertEntities = `INSERT INTO d_role VALUES(
	$1, $2, $3, $4, $5
	)`

func TestOpenClosePostgres(t *testing.T) {

	if env := NewEnv("postgres", "postgres://test:test@localhost/test").Open(); env.Err != nil {
		t.Errorf("Open database error : %d.", env.Err)
	} else {
		if env = env.Close(); env.Err != nil {
			t.Errorf("Close database error : %d.", env.Err)
		}
	}
}
func TestExecPostgres(t *testing.T) {

	if env := NewEnv("postgres", "postgres://test:test@localhost/test").Open(); env.Err != nil {
		t.Errorf("Open database error : %d.", env.Err)
	} else {
		//create := NewExec(createEntities)

		//if env = env.Exec(create); env.Err != nil {
		//		t.Errorf("Exec database error : %d.", env.Err)
		//}

		//env.Exec(NewExec(dropEntities))

		if env := env.Close(); env.Err != nil {
			t.Errorf("Close database error : %d.", env.Err)
		}
	}
}
func TestQueryPostgres(t *testing.T) {

	if env := NewEnv("postgres", "postgres://test:test@localhost/test").Open(); env.Err != nil {
		t.Errorf("Open database error : %d.", env.Err)
	} else {

		//create := NewExec(createEntities)

		//if env = env.Exec(create); env.Err != nil {
		//	t.Errorf("Exec database error : %d.", env.Err)
		//} else {
		insert := NewExec(insertEntities)
		if env = env.Exec(insert, guid.NewString(), time.Now(), time.Now(), nil, "TestRole1"); env.Err != nil {
			t.Errorf("Insert database error : %d.", env.Err)
		}
		//}

		query := NewQuery("select u_u_i_d, crt_at, upd_at, name from d_role where del_at is null")

		if env = env.Query(query); env.Err != nil {
			t.Errorf("Query database error : %d.", env.Err)
		}
		/*
			if cols, err := query.Rows.Columns(); err != nil {
				t.Errorf("Query Columns error : %d.", env.Err)
			} else {
				log.Printf("Columns: %v", cols)
			}

			if cols, err := query.Rows.ColumnTypes(); err != nil {
				t.Errorf("Query ColumnTypes error : %d.", env.Err)
			} else {
				for _, c := range cols {
					log.Println(c.Name(), c.DatabaseTypeName(), c.ScanType())
				}
			}
		*/
		if json, err := query.ToJSON(); err != nil {
			t.Errorf("ToJSON query error : %d.", err)
		} else {
			log.Println(string(json))
		}

		if query.Count() < 1 {
			t.Errorf("Count query error : %d.", query.Count())
		}

		//env.Exec(NewExec(dropEntities))

		if env := env.Close(); env.Err != nil {
			t.Errorf("Close database error : %d.", env.Err)
		}
	}
}

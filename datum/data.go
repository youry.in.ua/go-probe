package datum

import "time"

/*
// DStd - common datum fields
type DStd struct {
	UUID  string `xorm:"pk not null unique"`
	CrtAt time.Time
	UpdAt time.Time
	DelAt time.Time
}
*/

// DRole - role of objects
type DRole struct {
	UUID  string `xorm:"pk not null unique"`
	CrtAt time.Time
	UpdAt time.Time
	DelAt time.Time `xorm:"index"`
	Name  string    `xorm:"not null unique"`
}

// DRel - relationsheep
type DRel struct {
	UUID  string `xorm:"pk not null unique"`
	CrtAt time.Time
	UpdAt time.Time
	DelAt time.Time `xorm:"index"`
	Name  string    `xorm:"not null unique"`
}

// DRoleRel - relations of roles
type DRoleRel struct {
	UUID     string `xorm:"pk not null unique"`
	CrtAt    time.Time
	UpdAt    time.Time
	DelAt    time.Time `xorm:"index"`
	Rel      string    `xorm:"not null unique(role_rel)"`
	RoleFrom string    `xorm:"not null unique(role_rel)"`
	RoleTo   string    `xorm:"not null unique(role_rel)"`
}

// DProp - properties
type DProp struct {
	UUID    string `xorm:"pk not null unique"`
	CrtAt   time.Time
	UpdAt   time.Time
	DelAt   time.Time `xorm:"index"`
	Name    string    `xorm:"not null unique"`
	Type    string
	Default string
}

// DRoleProp - properties of roles
type DRoleProp struct {
	UUID    string `xorm:"pk not null unique"`
	CrtAt   time.Time
	UpdAt   time.Time
	DelAt   time.Time `xorm:"index"`
	Role    string    `xorm:"not null unique(role_prop)"`
	Prop    string    `xorm:"not null unique(role_prop)"`
	Default string
}

// DRelProp - properties of relations
type DRelProp struct {
	UUID    string `xorm:"pk not null unique"`
	CrtAt   time.Time
	UpdAt   time.Time
	DelAt   time.Time `xorm:"index"`
	Rel     string    `xorm:"not null unique(rel_prop)"`
	Prop    string    `xorm:"not null unique(rel_prop)"`
	Default string
}

// DObj - objects
type DObj struct {
	UUID  string `xorm:"pk not null unique"`
	CrtAt time.Time
	UpdAt time.Time
	DelAt time.Time `xorm:"index"`
}

// DRoleObj - objects of roles
type DRoleObj struct {
	UUID  string `xorm:"pk not null unique"`
	CrtAt time.Time
	UpdAt time.Time
	DelAt time.Time `xorm:"index"`
	Role  string    `xorm:"not null unique(role_obj)"`
	Obj   string    `xorm:"not null unique(role_obj)"`
}

// DObjProp - properties of objects
type DObjProp struct {
	UUID  string `xorm:"pk not null unique"`
	CrtAt time.Time
	UpdAt time.Time
	DelAt time.Time `xorm:"index"`
	Obj   string    `xorm:"not null unique(obj_prop)"`
	Prop  string    `xorm:"not null unique(obj_prop)"`
	Value string
}

// DObjRel - relations of objects
type DObjRel struct {
	UUID    string `xorm:"pk not null unique"`
	CrtAt   time.Time
	UpdAt   time.Time
	DelAt   time.Time `xorm:"index"`
	Rel     string    `xorm:"not null unique(obj_rel)"`
	ObjFrom string    `xorm:"not null unique(obj_rel)"`
	ObjTo   string    `xorm:"not null unique(obj_rel)"`
}

// DObjRelProp - properties of obj`s relations
type DObjRelProp struct {
	UUID    string `xorm:"pk not null unique"`
	CrtAt   time.Time
	UpdAt   time.Time
	DelAt   time.Time `xorm:"index"`
	RoleRel string    `xorm:"not null unique(obj_rel_prop)"`
	Prop    string    `xorm:"not null unique(obj_rel_prop)"`
	Value   string
}

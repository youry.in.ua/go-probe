package datum

// Model - entries to elements scheme
type Model struct {
	Env   *Env
	Roles map[string]*Role
	Rels  map[string]*Rel
	Props map[string]*Prop
	Err   error
}

// NewModel - constructor model
func NewModel() *Model {
	return &Model{}
}

// Open - open and load elements scheme
func (m *Model) Open(env *Env) *Model {
	m.Env = env
	m.Env.Open()
	return m
}

// Close - close elements scheme
func (m *Model) Close() *Model {
	m.Env.Close()
	return m
}

// Role - container for roles
type Role struct {
	Role    *DRole
	Prop    map[string]*DProp
	RelFrom map[string]*DRel
	RelTo   map[string]*DRel
	Err     error
}

// NewRole - constructor role
func NewRole(name string) *Role {
	return &Role{
		Role: &DRole{Name: name},
	}
}

// Rel - container for ralations
type Rel struct {
	Rel      *DRel
	Prop     map[string]*DProp
	RoleFrom map[string]*DRole
	RoleTo   map[string]*DRole
	RoleRel  map[string]*DRoleRel
	Err      error
}

// Prop - container for roles
type Prop struct {
	Prop *DProp
	Role map[string]*DRole
	Err  error
}

package datum

import (
	"database/sql"
	"encoding/json"

	"github.com/go-xorm/xorm"
	//_ "github.com/lib/pq"
	//"fmt"
	//"log"
)

// Env - environment instance
type Env struct {
	Driver string
	Source string
	//DB     *sql.DB
	Xorm *xorm.Engine
	Err  error
}

// Exec - execute contaner
type Exec struct {
	SQL    string
	Result sql.Result
	Err    error
}

// Query - query contaner
type Query struct {
	SQL string
	//Rows  *sql.Rows
	Array []map[string]interface{}
	Err   error
}

// NewEnv - create new Env instance
func NewEnv(driver string, source string) *Env {
	return &Env{Driver: driver, Source: source}
}

// Open  - open database
func (i *Env) Open() *Env {
	i.Xorm, i.Err = xorm.NewEngine(
		i.Driver,
		i.Source,
	)

	i.Xorm.Sync(new(DRole))
	i.Xorm.Sync(new(DProp))
	i.Xorm.Sync(new(DRoleProp))

	return i
}

// Close  - close database
func (i *Env) Close() *Env {
	i.Err = i.Xorm.Close()
	//i.Err = i.DB.Close()
	return i
}

// Query  - query database
func (i *Env) Query(query *Query, args ...interface{}) *Env {
	//a := []interface{}{query.SQL}
	//a = append(a, args...)
	//log.Println("Query", a)
	sess := i.Xorm.SQL(query.SQL, args...)
	query.Array, query.Err = sess.QueryInterface()
	//query.Array, query.Err = i.Xorm.Query(args...)
	//query.Rows, query.Err = i.DB.Query(query.SQL, args...)
	i.Err = query.Err
	sess.Close()
	return i
}

// Exec  - exec database
func (i *Env) Exec(exec *Exec, args ...interface{}) *Env {
	//log.Println("Exec", exec, args)
	exec.Result, exec.Err = i.Xorm.Exec(exec.SQL, args...)
	//exec.Result, exec.Err = i.DB.Exec(exec.SQL, args...)
	i.Err = exec.Err
	return i
}

// NewExec - create new exec container
func NewExec(exec string) *Exec {
	return &Exec{SQL: exec}
}

// NewQuery - create new query container
func NewQuery(query string) *Query {
	return &Query{SQL: query}
}

// Count - count rows in the query result
func (q *Query) Count() int {
	return len(q.Array)
}

// ToJSON - marshal array to JSON
func (q *Query) ToJSON() ([]byte, error) {
	b, err := json.Marshal(q.Array)
	return b, err
}
